$(function() {
    var acr = $('.accordion');
    var i;
    for (i = 0; i < acr.length; i++){
        $('.accordion').eq(i).click(function(){
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            } 
        });
    }
    
    $('.move-down').click(function() {
        var self = $(this);
          item = self.parents('div.element');
          swap = item.next();
        item.before(swap.detach());
    });
    $('.move-up').click(function() {
        var self = $(this);
            item = self.parents('div.element');
            swap = item.prev();
        item.after(swap.detach());
    });
    
    $('#switch').click(function(){
        // document.body.classList.toggle("change-mode"); boleh jg pke ini
        if($(".accordion").css("background-color") == "rgb(238, 238, 238)"){
            $("body").css({"background-color": "lightgray", "color":"darkblue"});
            $(".accordion").css({"background-color": "darkblue", "color":"white"});
            $(".panel").css({"background-color": "black", "color":"white"});
        }else{
            $("body").css({"background-color": "white", "color":"black"});
            $(".accordion").css({"background-color": "#eee", "color":"black"});
            $(".panel").css({"background-color": "white", "color":"black"});}
    });
});