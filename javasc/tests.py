from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import accordion
from .apps import JavascConfig
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time


class UnitTest(TestCase):
    def test_app(self):
        self.assertEqual(JavascConfig.name, "javasc")
    def test_urls_is_exist(self):
        response = Client().get('/aboutme/')
        self.assertEqual(response.status_code, 200)
    def test_event_using_template(self):
        response = Client().get('/aboutme/')
        self.assertTemplateUsed(response, 'accordion.html')
    def test_views1_func(self):
        found = resolve('/aboutme/')
        self.assertEqual(found.func, accordion)
    def test_title_page(self):
        response = Client().get('/aboutme/')
        self.assertContains(response, "Hello, It's")
    
class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080")
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path='./chromedriver.log'
        service_args=['--verbose']
        self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        time.sleep(1)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_accordion_work(self):
        selenium = self.browser
        selenium.get(self.live_server_url+'/aboutme/')

        # inisiasikan tiap element accordion
        element1 = selenium.find_element_by_id("acr1")
        element2 = selenium.find_element_by_id("acr2")
        element3 = selenium.find_element_by_id("acr3")
        element4 = selenium.find_element_by_id("acr4")
        panel_element1 = selenium.find_element_by_id("tab1")
        panel_element2 = selenium.find_element_by_id("tab2")
        panel_element3 = selenium.find_element_by_id("tab3")  
        panel_element4 = selenium.find_element_by_id("tab4")
        
        #membuktikan bahwa jika diklik, maka akan membuka panelnya
        element1.click()
        self.assertEqual("block", panel_element1.value_of_css_property("display")) 
        element2.click()
        self.assertEqual("block", panel_element2.value_of_css_property("display")) 
        element3.click()
        self.assertEqual("block", panel_element3.value_of_css_property("display")) 
        element4.click()
        self.assertEqual("block", panel_element4.value_of_css_property("display"))

    def test_accordion_up_down(self):
        selenium = self.browser
        selenium.get(self.live_server_url+'/aboutme/')

        #inisiasikan salah satu element accordion
        element2 = selenium.find_element_by_id("acr2")
        up_element2 = selenium.find_element_by_class_name("up2")
        down_element2 = selenium.find_element_by_class_name("down2")

        #membuktikan bahwa letak asalnya pada urutan 2
        self.assertEqual("2", element2.value_of_css_property("order"))

        #setelah diklik up, berpindah menjadi urutan ke 1
        up_element2.click()

        #membuktikan bahwa setelah diklik down, kembali berpindah menjadi urutan ke 2 (asal)
        down_element2.click()
        self.assertEqual("2", element2.value_of_css_property("order"))
    
    def test_change_color(self):
        selenium = self.browser
        selenium.get(self.live_server_url+'/aboutme/')
        
        #inisiasikan sebelum klik change color
        switch = selenium.find_element_by_id('switch')
        # body1 = selenium.find_element_by_tag_name("body").value_of_css_property("background-color")
        before = selenium.find_element_by_class_name("accordion").value_of_css_property("background-color")

        #membuktikan terjadinya perubahan setelah diklik
        switch.click()
        # body2 = selenium.find_element_by_tag_name("body").value_of_css_property("background-color")
        after = selenium.find_element_by_class_name("accordion").value_of_css_property("background-color")
        
        # self.assertNotEqual(body1, body2)
        self.assertNotEqual(before, after)
