from django.urls import path
from . import views

app_name = 'javasc'

urlpatterns = [
    path('', views.accordion, name='accordion'),
]