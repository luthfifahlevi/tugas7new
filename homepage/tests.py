from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import index, konfirmasi
from .apps import HomepageConfig
from .models import Messages
from .forms import Messages_Form
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time


class UnitTest(TestCase):
    #app
    def test_app(self):
        self.assertEqual(HomepageConfig.name, "homepage")

    #index.html
    def test_index_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_index_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    def test_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    #konfirmasi.html
    def test_konfirmasi_url_is_exist(self):
        response = Client().post('konfirmasi/')
    def test_konfirmasi_func(self):
        found = resolve('/konfirmasi/')
        self.assertEqual(found.func, konfirmasi)

    #models
    def test_model_can_create_and_check_html(self):
        message = Messages.objects.create(nama="Luthfi", pesan="Lari kuy")
        counting_all_available_todo = Messages.objects.all().count()
        response = Client().get('/')
        self.assertContains(response, "Luthfi", count = 1, html=True)
        self.assertContains(response, "Lari kuy", count = 1, html=True)
        self.assertEqual(counting_all_available_todo, 1)
        self.assertIsInstance(message, Messages)
        
    def test_model_can_print(self):
        message = Messages.objects.create(nama="Luthfi", pesan="Lari kuy")
        self.assertEqual(message.__str__(), "Luthfi")
    
    #forms
    def test_form(self):
        form_data = {'nama': 'Luthfi', 'pesan': 'just test'}
        form = Messages_Form(data=form_data)
        self.assertTrue(form.is_valid())
        form_data={}
        form = Messages_Form(data=form_data)
        self.assertFalse(form.is_valid())
    
class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path='./chromedriver.log'
        service_args=['--verbose']
        self.browser = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
        # time.sleep(3)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()
    
    def test_say_yes(self):
        #mengisi form
        selenium = self.browser 
        selenium.get(self.live_server_url + '/')
        nama = selenium.find_element_by_id('id_nama')
        pesan = selenium.find_element_by_id('id_pesan')
        submit = selenium.find_element_by_class_name('btn-success')
        nama.send_keys('Luthfi')
        pesan.send_keys('Just test')
        submit.send_keys(Keys.RETURN)
        # time.sleep(3)

        #konfirmasi pesan
        self.assertIn('Just test', selenium.page_source)
        self.assertIn('Luthfi', selenium.page_source)
        # time.sleep(3)

        #jika setuju
        setuju = selenium.find_element_by_id('Ya')
        setuju.send_keys(Keys.RETURN)
        self.assertIn('Luthfi', selenium.page_source)
        self.assertIn('Just test', selenium.page_source)
        # time.sleep(3)

    def test_say_no(self):
        #mengisi form
        selenium = self.browser 
        selenium.get(self.live_server_url + '/')
        nama = selenium.find_element_by_id('id_nama')
        pesan = selenium.find_element_by_id('id_pesan')
        submit = selenium.find_element_by_class_name('btn-success')
        nama.send_keys('Naruto')
        pesan.send_keys('Tes salah')
        submit.send_keys(Keys.RETURN) #ke http://localhost:8000/konfirmasi/
        # time.sleep(3)

        #konfirmasi pesan
        self.assertIn('Tes salah', selenium.page_source)
        self.assertIn('Naruto', selenium.page_source)
        # time.sleep(3)

        #jika tidak
        tidak_setuju = selenium.find_element_by_id('Tidak')
        tidak_setuju.send_keys(Keys.RETURN)
        self.assertNotIn('Naruto', selenium.page_source)
        self.assertNotIn('Tes salah', selenium.page_source)
        # time.sleep(3)
    
    def test_color(self):
        selenium = self.browser 
        selenium.get(self.live_server_url + '/')
        nama = selenium.find_element_by_id('id_nama')
        pesan = selenium.find_element_by_id('id_pesan')
        submit = selenium.find_element_by_class_name('btn-success')
        nama.send_keys('Naruto')
        pesan.send_keys('Tes salah')
        submit.send_keys(Keys.RETURN) #ke http://localhost:8000/konfirmasi/
        # time.sleep(3)
        
        setuju = selenium.find_element_by_id('Ya')
        setuju.send_keys(Keys.RETURN)

        #menginisiasi data background sebelum diubah
        before = selenium.find_element_by_class_name('media').value_of_css_property('background-color')

        #disubmit untuk mengganti background
        color = selenium.find_element_by_name('submits')
        color.click()

        after = selenium.find_element_by_class_name('media').value_of_css_property('background-color')

        self.assertNotEqual(before, after)
