from django.db import models

class Messages(models.Model):
    nama = models.CharField(max_length = 100)
    pesan = models.TextField()
    color = models.CharField(max_length=100, default='#000000')

    def __str__(self):
        return self.nama