from django import forms
from .models import Messages

class Messages_Form(forms.ModelForm):
    class Meta:
        model = Messages
        fields = ["nama","pesan"]
        labels = {
            'nama' : 'Nama',
            'pesan' : 'Pesan/Status',
        }
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
        }
        )