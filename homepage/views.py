from django.shortcuts import render, redirect
from .models import Messages
from .forms import Messages_Form
import random

def index(request):
    form = Messages_Form()
    if request.method == 'POST':
        nama = request.POST['nama']
        pesan = request.POST['pesan']
        Messages.objects.create(nama=nama,pesan=pesan)
    objs = Messages.objects.all().order_by('id')
    return render(request,'index.html',{'form': form, 'objs':objs})

def konfirmasi(request):
    if request.method == "POST":
        form = Messages_Form(request.POST or None)
        return render(request, 'konfirmasi.html', {'nama':form.data['nama'], 'pesan':form.data['pesan']})

def ubah(request, id):
    if request.method == "POST":
        form = Messages.objects.get(pk = id)
        a = str(hex(random.randint(0,255)))[2:]
        b = str(hex(random.randint(0,255)))[2:]
        c = str(hex(random.randint(0,255)))[2:]
        color = a+b+c
        new_color = "#" + color
        form.color = new_color
        form.save()
        return redirect('/')
